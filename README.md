# README #

URL to access the webservers: http://stan.gwtech.com.br or https://stan.gwtech.com.br

Load balancers URL: uat-StanElasticLoa-SZD2MZ08GVCT-2131339211.ap-southeast-2.elb.amazonaws.com

How to upload the self signed key to IAM and get the arn:.
http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/configuring-https-ssl-upload.html

The cloudformation script in JSON format is located on aws folder named as stan.cloudformation.json

Eduardo Rosas
e-mail: erosas.ti@gmail.com
Mobile: 0415326552


Please let me know when you finish the validation to delete the stack on Amazon.
